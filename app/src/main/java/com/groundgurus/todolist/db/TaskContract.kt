package com.groundgurus.todolist.db

import android.provider.BaseColumns

object TaskContract {
    const val DB_NAME: String = "com.groundgurus.todolist.db"
    const val DB_VERSION: Int = 1

    object TaskEntry : BaseColumns {
        const val TABLE: String = "tasks"
        const val ID: String = "id"
        const val COL_TASK_TITLE: String = "title"
    }
}