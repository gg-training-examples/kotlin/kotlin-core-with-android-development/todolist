package com.groundgurus.todolist

import android.content.ContentValues
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.*
import com.groundgurus.todolist.db.TaskContract
import com.groundgurus.todolist.db.TaskDbHelper

class MainActivity : AppCompatActivity() {
    lateinit var mTaskListView: ListView
    private var mAdapter: ArrayAdapter<String>? = null
    private lateinit var mHelper: TaskDbHelper
    lateinit var taskDelete: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mTaskListView = findViewById(R.id.list_todo)

        mAdapter = ArrayAdapter(this, R.layout.activity_todo, R.id.task_title)

        mTaskListView.adapter = mAdapter

        mHelper = TaskDbHelper(this)

        updateUI()
    }

    private fun updateUI() {
        val db = mHelper.readableDatabase
        val cursor = db.query(
            TaskContract.TaskEntry.TABLE,
            arrayOf(TaskContract.TaskEntry.ID, TaskContract.TaskEntry.COL_TASK_TITLE),
            null, null, null, null, null
        )

        val taskList = mutableListOf<String>()
        while (cursor.moveToNext()) {
            val index = cursor.getColumnIndex(TaskContract.TaskEntry.COL_TASK_TITLE)
            taskList.add(cursor.getString(index))
        }

        if (mAdapter == null) {
            mAdapter = ArrayAdapter(this, R.layout.activity_todo, R.id.task_title, taskList)
            mTaskListView.adapter = mAdapter
        } else {
            mAdapter?.clear()
            mAdapter?.addAll(taskList)
            mAdapter?.notifyDataSetChanged()
        }

        cursor.close()
        db.close()
    }

    fun deleteTask(view: View) {
        taskDelete = view.findViewById(R.id.task_delete)
        taskDelete.setOnClickListener {
            val parent = it.parent as View
            val taskTextView = parent.findViewById<TextView>(R.id.task_title)
            val task = taskTextView.text.toString()

            // delete the record in the database
            val db = mHelper.writableDatabase
            db.delete(TaskContract.TaskEntry.TABLE, "${TaskContract.TaskEntry.COL_TASK_TITLE} = ?", arrayOf(task))
            db.close()

            updateUI()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.action_add_task -> {
                val taskEditText = EditText(this)

                val dialog = AlertDialog.Builder(this)
                    .setTitle("Add a new task")
                    .setView(taskEditText)
                    .setPositiveButton("Add") { _, _ ->
                        run {
                            val task = taskEditText.text.toString()
                            val db = mHelper.writableDatabase
                            val values = ContentValues()

                            values.put(TaskContract.TaskEntry.COL_TASK_TITLE, task)
                            db.insertWithOnConflict(
                                TaskContract.TaskEntry.TABLE, null, values,
                                SQLiteDatabase.CONFLICT_REPLACE
                            )
                            db.close()

                            updateUI()
                        }
                    }
                    .create()

                dialog.show()

                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }
}
